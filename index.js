var canvas = document.querySelector("canvas");
var tilesetContainer = document.querySelector(".tileset-container");
var tilesetSelection = document.querySelector(".tileset-container_selection");
var tilesetImage = document.querySelector("#tileset-source");
var currentImgContext = "grass-dirt";

var selection = [0, 0]; //Which tile we will paint from the menu

var isMouseDown = false;
var currentLayer = 0;
var contextsLayer = ["grass-dirt", "sand-water", "dirt-sand", "water-sand", "rock-lava", "lava-rock"]

var grassDirt = new Image();
var sandWater = new Image();
var dirtSand = new Image();
var waterSand = new Image();
var rockLave = new Image();
var lavaRock = new Image();
grassDirt.crossOrigin = "anonymous";
sandWater.crossOrigin = "anonymous";
dirtSand.crossOrigin = "anonymous";
waterSand.crossOrigin = "anonymous";
rockLave.crossOrigin = "anonymous";
lavaRock.crossOrigin = "anonymous";

grassDirt.src = "https://i.ibb.co/pXBYCcb/tile-grass-dirt.png";
sandWater.src = "https://i.ibb.co/41KJHM2/tile-island-water.png";
dirtSand.src = "https://i.ibb.co/BL73Vkh/tile-dirt-grass.png";
waterSand.src = "https://i.ibb.co/gPbtHN2/tile-water-island.png";
rockLave.src = "https://i.ibb.co/gv0c1ZT/tile-rock-lava.png";
lavaRock.src = "https://i.ibb.co/mv3x8Rk/tile-lava-rock.png";
var images = {
    "grass-dirt": grassDirt,
    "sand-water": sandWater,
    "dirt-sand": dirtSand,
    "water-sand": waterSand,
    "rock-lava": rockLave,
    "lava-rock": lavaRock,
}
var layers = {
    "grass-dirt": [{}, {}, {}],
    "sand-water": [{}, {}, {}],
    "dirt-sand": [{}, {}, {}],
    "water-sand": [{}, {}, {}],
    "rock-lava": [{}, {}, {}],
    "lava-rock": [{}, {}, {}]
};

//Select tile from the Tiles grid
tilesetContainer.addEventListener("mousedown", (event) => {
    selection = getCoords(event);
    tilesetSelection.style.left = selection[0] * 32 + "px";
    tilesetSelection.style.top = selection[1] * 32 + "px";
});

//Handler for placing new tiles on the map
function addTile(mouseEvent) {
    var clicked = getCoords(event);
    var key = clicked[0] + "-" + clicked[1];

    if (mouseEvent.shiftKey) {
        delete layers[currentImgContext][currentLayer][key];
    } else {
        layers[currentImgContext][currentLayer][key] = [selection[0], selection[1]];
    }
    draw();
}

//Bind mouse events for painting (or removing) tiles on click/drag
canvas.addEventListener("mousedown", () => {
    isMouseDown = true;
});
canvas.addEventListener("mouseup", () => {
    isMouseDown = false;
});
canvas.addEventListener("mouseleave", () => {
    isMouseDown = false;
});
canvas.addEventListener("mousedown", addTile);
canvas.addEventListener("mousemove", (event) => {
    if (isMouseDown) {
        addTile(event);
    }
});

//Utility for getting coordinates of mouse click
function getCoords(e) {
    const { x, y } = e.target.getBoundingClientRect();
    const mouseX = e.clientX - x;
    const mouseY = e.clientY - y;
    return [Math.floor(mouseX / 32), Math.floor(mouseY / 32)];
}

//converts data to image:data string and pipes into new browser tab
function exportImage() {
    var data = canvas.toDataURL();
    var image = new Image();
    image.src = data;

    var w = window.open("");
    w.document.write(image.outerHTML);
    w.document.location.href = '#';
    w.document.close();
}

//Reset state to empty
function clearCanvas() {
    layers = {
        "grass-dirt": [{}, {}, {}],
        "sand-water": [{}, {}, {}],
        "dirt-sand": [{}, {}, {}],
        "water-sand": [{}, {}, {}],
        "rock-lava": [{}, {}, {}],
        "lava-rock": [{}, {}, {}]
    }
    draw();
}

function setLayer(newLayer) {
    //Update the layer
    currentLayer = newLayer;

    //Update the UI to show updated layer
    var oldActiveLayer = document.querySelector(".layer.active");
    if (oldActiveLayer) {
        oldActiveLayer.classList.remove("active");
    }
    document.querySelector(`[tile-layer="${currentLayer}"]`).classList.add("active");
}

function draw() {
    var ctx = canvas.getContext("2d");
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    var size_of_crop = 32;
    contextsLayer.forEach(context => {
        layers[context].forEach((layer) => {
            Object.keys(layer).forEach((key) => {
                //Determine x/y position of this placement from key ("3-4" -> x=3, y=4)
                var positionX = Number(key.split("-")[0]);
                var positionY = Number(key.split("-")[1]);
                var [tilesheetX, tilesheetY] = layer[key];
                var canvasImageSource = images[context];
                ctx.drawImage(
                    canvasImageSource,
                    tilesheetX * 32,
                    tilesheetY * 32,
                    size_of_crop,
                    size_of_crop,
                    positionX * 32,
                    positionY * 32,
                    size_of_crop,
                    size_of_crop
                );
            });
        });
    });
    console.log('finish');
}

//Default image for booting up -> Just looks nicer than loading empty canvas

//Initialize app when tileset source is done loading

tilesetImage.src = "tile_grass_dirt.png";


var tilesets = [
    { label: "grass-dirt", path: "https://i.ibb.co/pXBYCcb/tile-grass-dirt.png" },
    { label: "sand-water", path: "https://i.ibb.co/41KJHM2/tile-island-water.png" },
    { label: "dirt-sand", path: "https://i.ibb.co/BL73Vkh/tile-dirt-grass.png" },
    { label: "water-sand", path: "https://i.ibb.co/gPbtHN2/tile-water-island.png" },
    { label: "rock-lava", path: "https://i.ibb.co/gv0c1ZT/tile-rock-lava.png" },
    { label: "lava-rock", path: "https://i.ibb.co/mv3x8Rk/tile-lava-rock.png" }
];


var sel = document.getElementById('tileset-select');
var fragment = document.createDocumentFragment();

tilesets.forEach(function (tileset, index) {
    var opt = document.createElement('option');
    opt.innerHTML = tileset.label;
    opt.value = tileset.path;
    fragment.appendChild(opt);
});

sel.appendChild(fragment);

function val() {
    d = document.getElementById("tileset-select").value;
    currentImgContext = document.getElementById("tileset-select").options[document.getElementById("tileset-select").selectedIndex].label;//get the selected option text
    tilesetImage.src = d;
}